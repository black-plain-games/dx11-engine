#pragma once

#include "ICamera.h"

namespace BlackPlain
{
	class Camera : public ICamera
	{
	public:
		Camera(float fieldOfView, float viewWidth, float viewHeight, float nearPlane, float farPlane);

		virtual void Update();
			    
		virtual void SetFieldOfView(float fieldOfView);
		virtual void SetViewDimensions(float width, float height);
		virtual void SetNearPlane(float nearPlane);
		virtual void SetFarPlane(float farPlane);
			    
		virtual void Rotate(float roll, float pitch, float yaw);
		virtual void Rotate(XMFLOAT3 delta);
			    
		virtual void MovePosition(float x, float y, float z);
		virtual void MovePosition(XMFLOAT3 offset);
			    
		virtual void MoveTarget(float x, float y, float z);
		virtual void MoveTarget(XMFLOAT3 offset);
			    
		virtual void Move(float x, float y, float z);
		virtual void Move(XMFLOAT3 offset);

		virtual XMMATRIX GetWorld() const { return _world; }
		virtual XMMATRIX GetProjection() const { return _projection; }
		virtual XMMATRIX GetView() const { return _view; }
		virtual XMMATRIX GetOrthographic() const { return _orthographic; }

	private:
		float _fieldOfView;
		float _viewWidth, _viewHeight;
		float _nearPlane, _farPlane;

		float _roll, _pitch, _yaw;

		XMFLOAT3 _up, _position, _target;

		bool _isWorldDirty, _isProjectionDirty, _isViewDirty, _isOrthographicDirty;

		XMMATRIX _world, _projection, _view, _orthographic;
	};
}