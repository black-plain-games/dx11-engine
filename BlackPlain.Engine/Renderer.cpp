#include "Renderer.h"

#include "Utilities.h"

#include <d3d11.h>
#include <DirectXMath.h>

using namespace BlackPlain;
using namespace DirectX;

Renderer::Renderer()
{
	_renderGroups = std::map<RenderEffect*, std::vector<IRenderable*>, bool (*)(RenderEffect*, RenderEffect*)>(&Compare);
	memset(&_clearColor, 0, sizeof(float) * 4);
	_windowDimensions = XMFLOAT2(0, 0);
	_swapChain = nullptr;
	_device = nullptr;
	_deviceContext = nullptr;
	_backBuffer = nullptr;
	_renderTargetView = nullptr;
	_depthStencilBuffer = nullptr;
	_depthStencilState2D = nullptr;
	_depthStencilState3D = nullptr;
	_currentDepthStencilState = nullptr;
	_depthStencilView = nullptr;
	_rasterState = nullptr;
	_viewport = D3D11_VIEWPORT();
}

D3D11_DEPTH_STENCIL_DESC GetDepthStencilDescription(bool depthEnable)
{
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));

	depthStencilDesc.DepthEnable = depthEnable;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;

	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	return depthStencilDesc;
}

void Renderer::Initialize(unsigned int windowWidth, unsigned int windowHeight, HWND windowHandle)
{
	IDXGIFactory* factory;
	auto result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
	THROWFAST(result, "Failed to create DXGI factory");

	IDXGIAdapter* adapter;
	result = factory->EnumAdapters(0, &adapter);
	THROWFAST(result, "Failed to enumeration graphics adapters");

	IDXGIOutput* adapterOutput;
	result = adapter->EnumOutputs(0, &adapterOutput);
	THROWFAST(result, "Failed to enumerate adapter outputs");

	UINT modeCount = 0;
	result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &modeCount, nullptr);
	THROWFAST(result, "Failed to get display mode count");

	auto displayModeList = new DXGI_MODE_DESC[modeCount];
	result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &modeCount, displayModeList);
	THROWFAST(result, "Failed to get display modes");

	auto numerator = 0;
	auto denominator = 0;
	auto maxRefreshRate = 0.0f;
	for (unsigned int index = 0; index < modeCount; index++)
	{
		auto displayMode = &displayModeList[index];
		auto refreshRate = (float)displayMode->RefreshRate.Numerator / (float)displayMode->RefreshRate.Denominator;

		if (displayMode->Width == windowWidth &&
			displayMode->Height == windowHeight &&
			refreshRate > maxRefreshRate)
		{
			numerator = displayMode->RefreshRate.Numerator;
			denominator = displayMode->RefreshRate.Denominator;
			maxRefreshRate = refreshRate;
		}
	}

	_windowDimensions = XMFLOAT2(windowWidth, windowHeight);

	DXGI_ADAPTER_DESC adapterDesc;
	result = adapter->GetDesc(&adapterDesc);
	THROWFAST(result, "Failed to get adapter description");

	delete[] displayModeList;
	SAFERELEASE(adapterOutput);
	SAFERELEASE(adapter);
	SAFERELEASE(factory);

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Width = windowWidth;
	swapChainDesc.BufferDesc.Height = windowHeight;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = denominator;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = windowHandle;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.Windowed = true;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Flags = 0;

	auto featureLevels = new D3D_FEATURE_LEVEL[1]{ D3D_FEATURE_LEVEL_11_0 };
	result = D3D11CreateDeviceAndSwapChain(
		nullptr,
		D3D_DRIVER_TYPE_HARDWARE,
		nullptr,
		0,
		featureLevels,
		1,
		D3D11_SDK_VERSION,
		&swapChainDesc,
		&_swapChain,
		&_device,
		nullptr,
		&_deviceContext);
	THROWFAST(result, "Failed to create device and swap chain");

	result = _swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&_backBuffer);
	THROWFAST(result, "Failed to get backbuffer");

	result = _device->CreateRenderTargetView(_backBuffer, nullptr, &_renderTargetView);
	THROWFAST(result, "Failed to create render target view");

	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));

	depthBufferDesc.Width = windowWidth;
	depthBufferDesc.Height = windowHeight;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	result = _device->CreateTexture2D(&depthBufferDesc, nullptr, &_depthStencilBuffer);
	THROWFAST(result, "Failed to create depth stencil buffer");

	auto depthStencil2dDesc = GetDepthStencilDescription(false);

	result = _device->CreateDepthStencilState(&depthStencil2dDesc, &_depthStencilState2D);
	THROWFAST(result, "Failed to create 2D depth stencil state");

	auto depthStencil3dDesc = GetDepthStencilDescription(true);

	result = _device->CreateDepthStencilState(&depthStencil3dDesc, &_depthStencilState3D);
	THROWFAST(result, "Failed to create 3D depth stencil state");

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	result = _device->CreateDepthStencilView(
		_depthStencilBuffer,
		&depthStencilViewDesc,
		&_depthStencilView);
	THROWFAST(result, "Failed to create depth stencil view");

	_deviceContext->OMSetRenderTargets(1, &_renderTargetView, _depthStencilView);

	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	result = _device->CreateRasterizerState(&rasterDesc, &_rasterState);
	THROWFAST(result, "Failed to create rasterizer state");

	D3D11_BLEND_DESC blendStateDesc;
	ZeroMemory(&blendStateDesc, sizeof(D3D11_BLEND_DESC));
	blendStateDesc.RenderTarget[0].BlendEnable = true;
	blendStateDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendStateDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendStateDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendStateDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
	blendStateDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_DEST_ALPHA;
	blendStateDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendStateDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	ID3D11BlendState* blendState = nullptr;

	result = _device->CreateBlendState(&blendStateDesc, &blendState);
	THROWFAST(result, "Failed to create blend state");

	_deviceContext->OMSetBlendState(blendState, nullptr, 0xFFFFFF);

	SAFERELEASE(blendState);

	_viewport.Width = (float)windowWidth;
	_viewport.Height = (float)windowHeight;
	_viewport.MinDepth = 0.0f;
	_viewport.MaxDepth = 1.0f;
	_viewport.TopLeftX = 0.0f;
	_viewport.TopLeftY = 0.0f;

	_deviceContext->RSSetViewports(1, &_viewport);
}

void Renderer::Uninitialize()
{
	SAFERELEASE(_rasterState);
	SAFERELEASE(_depthStencilView);
	SAFERELEASE(_depthStencilState2D);
	SAFERELEASE(_depthStencilState3D);
	SAFERELEASE(_depthStencilBuffer);
	SAFERELEASE(_renderTargetView);
	SAFERELEASE(_deviceContext);
	SAFERELEASE(_device);
	SAFERELEASE(_swapChain);
}

void Renderer::SetClearColor(float color[4])
{
	_clearColor[0] = color[0];
	_clearColor[1] = color[1];
	_clearColor[2] = color[2];
	_clearColor[3] = color[3];
}

void Renderer::Render()
{
	_deviceContext->ClearRenderTargetView(_renderTargetView, _clearColor);
	_deviceContext->ClearDepthStencilView(_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	
	for (auto renderGroup : _renderGroups)
	{
		renderGroup.first->Apply();

		if (renderGroup.first->Is2D())
		{
			if (_currentDepthStencilState != _depthStencilState2D)
			{
				_deviceContext->OMSetDepthStencilState(_depthStencilState2D, 1);
				_currentDepthStencilState = _depthStencilState2D;
			}
		}
		else if(_currentDepthStencilState != _depthStencilState3D)
		{
			_deviceContext->OMSetDepthStencilState(_depthStencilState3D, 1);
			_currentDepthStencilState = _depthStencilState3D;
		}

		for (auto renderable : renderGroup.second)
		{
			renderable->Render();
		}
	}

	_swapChain->Present(1, 0);
}

void Renderer::SetVertexBuffer(ID3D11Buffer* buffer, unsigned int stride)
{
	unsigned int offset = 0;

	_deviceContext->IASetVertexBuffers(0, 1, &buffer, &stride, &offset);
}

void Renderer::SetIndexBuffer(ID3D11Buffer* buffer)
{
	_deviceContext->IASetIndexBuffer(buffer, DXGI_FORMAT_R32_UINT, 0);
}

void Renderer::SetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY topology)
{
	_deviceContext->IASetPrimitiveTopology(topology);
}

void Renderer::DrawIndexedPrimitive(int indexCount)
{
	_deviceContext->DrawIndexed(indexCount, 0, 0);
}

ID3D11VertexShader* Renderer::CreateVertexShader(void* data, size_t length)
{
	ID3D11VertexShader* output;

	auto result = _device->CreateVertexShader(
		(void*)data,
		length,
		nullptr,
		&output);

	THROWFAST(result, "Could not create vertex shader");

	return output;
}

ID3D11InputLayout* Renderer::CreateInputLayout(void* data, size_t length, D3D11_INPUT_ELEMENT_DESC* layoutDescription, unsigned int layoutLength)
{
	ID3D11InputLayout* output;

	auto result = _device->CreateInputLayout(
		layoutDescription,
		layoutLength,
		data,
		length,
		&output);

	THROWFAST(result, "Could not create input layout");

	return output;
}

ID3D11PixelShader* Renderer::CreatePixelShader(void* data, size_t length)
{
	ID3D11PixelShader* output;

	auto result = _device->CreatePixelShader(
		(void*)data,
		length,
		nullptr,
		&output);

	THROWFAST(result, "Could not create pixel shader");

	return output;
}

void Renderer::SetInputLayout(ID3D11InputLayout* layout)
{
	_deviceContext->IASetInputLayout(layout);
}

void Renderer::SetVertexShader(ID3D11VertexShader* shader)
{
	_deviceContext->VSSetShader(shader, nullptr, 0);
}

void Renderer::SetPixelShader(ID3D11PixelShader* shader)
{
	_deviceContext->PSSetShader(shader, nullptr, 0);
}

void Renderer::SetPixelShaderSamplers(unsigned int count, ID3D11SamplerState** samplers)
{
	_deviceContext->PSSetSamplers(0, count, samplers);
}

ID3D11SamplerState** Renderer::CreateSamplerStates(D3D11_SAMPLER_DESC* descriptions, unsigned int descriptionCount)
{
	auto samplerStates = new ID3D11SamplerState * [descriptionCount];

	for (unsigned int index = 0; index < descriptionCount; index++)
	{
		auto result = _device->CreateSamplerState(&descriptions[index], &samplerStates[index]);
		THROWFAST(result, 0);
	}

	return samplerStates;
}

void Renderer::SetVertexShaderConstantBuffers(unsigned int count, ID3D11Buffer** buffers)
{
	_deviceContext->VSSetConstantBuffers(0, count, buffers);
}

void Renderer::SetPixelShaderResources(unsigned int count, ID3D11ShaderResourceView** resources)
{
	_deviceContext->PSSetShaderResources(0, count, resources);
}

ID3D11Buffer* Renderer::CreateBuffer(D3D11_BUFFER_DESC description)
{
	return CreateBuffer(description, nullptr);
}

ID3D11Buffer* Renderer::CreateBuffer(D3D11_BUFFER_DESC description, D3D11_SUBRESOURCE_DATA* data)
{
	ID3D11Buffer* output;
	auto result = _device->CreateBuffer(&description, data, &output);
	THROWFAST(result, "Could not create buffer");
	return output;
}

void Renderer::UpdateBuffer(void* data, size_t dataLength, ID3D11Buffer* buffer)
{
	D3D11_MAPPED_SUBRESOURCE resource;

	auto result = _deviceContext->Map(buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
	THROWFAST(result, "Could not map buffer");

	memcpy(resource.pData, data, dataLength);

	_deviceContext->Unmap(buffer, 0);
}

ID3D11ShaderResourceView* Renderer::CreateShaderResourceView(ID3D11Resource* resource)
{
	D3D11_SHADER_RESOURCE_VIEW_DESC description;

	description.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	description.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	description.Texture2D.MostDetailedMip = 0;
	description.Texture2D.MipLevels = -1;

	ID3D11ShaderResourceView* output;
	auto result = _device->CreateShaderResourceView(resource, &description, &output);
	THROWFAST(result, "Failed to create shader resource view");

	_deviceContext->GenerateMips(output);

	return output;
}

ID3D11Texture2D* Renderer::CreateTexture(unsigned int width, unsigned int height, unsigned char* data)
{
	D3D11_TEXTURE2D_DESC textureDesc;
	textureDesc.Height = height;
	textureDesc.Width = width;
	textureDesc.MipLevels = 0;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

	ID3D11Texture2D* texture;
	auto result = _device->CreateTexture2D(&textureDesc, nullptr, &texture);
	FAILFAST(result, 0);

	auto rowPitch = width * 4 * sizeof(unsigned char);
	_deviceContext->UpdateSubresource(texture, 0, nullptr, data, rowPitch, 0);

	return texture;
}

ID3D11Texture2D* Renderer::CreateTexture(Utilities::ImageData& imageData)
{
	return CreateTexture(imageData.Width, imageData.Height, imageData.Data);
}

bool BlackPlain::Renderer::Compare(RenderEffect* a, RenderEffect* b)
{
	if (a == nullptr && b == nullptr)
	{
		return false;
	}

	if (a == nullptr)
	{
		return false;
	}

	if (b == nullptr)
	{
		return true;
	}

	return a->GetPriority() < b->GetPriority();
}
