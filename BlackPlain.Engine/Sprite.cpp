#include "Sprite.h"

#include "PositionVertex.h"

using namespace BlackPlain;

BlackPlain::Sprite::Sprite(BlackPlain::IRenderer* renderer)
	: _renderer(renderer)
{
	_positionX = 0;
	_positionY = 0;
	_width = 0;
	_height = 0;
	_scaleX = 1;
	_scaleY = 1;
	_isDirty = false;
	_indexBuffer = nullptr;
	_vertexBuffer = nullptr;
}

BlackPlain::Sprite::~Sprite()
{
	SAFERELEASE(_indexBuffer);
	SAFERELEASE(_vertexBuffer);
}

void BlackPlain::Sprite::Initialize()
{
	const int indexCount = 6;
	auto indices = new unsigned long[indexCount] { 2, 1, 0, 3, 2, 0 };

	auto indexBufferDesc = D3D11_BUFFER_DESC();
	ZeroMemory(&indexBufferDesc, sizeof(D3D11_BUFFER_DESC));

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA indexData;
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	_indexBuffer = _renderer->CreateBuffer(indexBufferDesc, &indexData);

	delete[] indices;

	const int vertexCount = 4;
	UpdateVertices();

	auto vertexBufferDesc = D3D11_BUFFER_DESC();
	ZeroMemory(&vertexBufferDesc, sizeof(D3D11_BUFFER_DESC));

	vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(BlackPlain::TexturedVertex) * vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexData;
	vertexData.pSysMem = _vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	_vertexBuffer = _renderer->CreateBuffer(vertexBufferDesc, &vertexData);
}

void BlackPlain::Sprite::SetPosition(float x, float y)
{
	_positionX = x;
	_positionY = y;
	_isDirty = true;
}

void BlackPlain::Sprite::Move(float xOffset, float yOffset)
{
	_positionX += xOffset;
	_positionY += yOffset;
	_isDirty = true;
}

void BlackPlain::Sprite::SetDimensions(float width, float height)
{
	_width = width;
	_height = height;
	_isDirty = true;
}

void BlackPlain::Sprite::SetScale(float x, float y)
{
	_scaleX = x;
	_scaleY = y;
	_isDirty = true;
}

inline void BlackPlain::Sprite::SetScale(float uniformScale)
{
	SetScale(uniformScale, uniformScale);
}

void BlackPlain::Sprite::Update()
{
	if (!_isDirty)
	{
		return;
	}

	UpdateVertices();

	_renderer->UpdateBuffer(_vertices, sizeof(BlackPlain::TexturedVertex) * 4, _vertexBuffer);

	_isDirty = false;
}

void BlackPlain::Sprite::Render()
{
	_renderer->SetVertexBuffer(_vertexBuffer, sizeof(BlackPlain::TexturedVertex));
	_renderer->SetIndexBuffer(_indexBuffer);
	_renderer->DrawIndexedPrimitive(6);
}

void BlackPlain::Sprite::UpdateVertices()
{
	auto width = _width * _scaleX;
	auto height = _height * _scaleY;

	_vertices[0].Position = XMFLOAT3(_positionX, _positionY, 0);
	_vertices[0].Texture = XMFLOAT2(0, 0);

	_vertices[1].Position = XMFLOAT3(_positionX + width, _positionY, 0);
	_vertices[1].Texture = XMFLOAT2(1, 0);

	_vertices[2].Position = XMFLOAT3(_positionX + width, _positionY + height, 0);
	_vertices[2].Texture = XMFLOAT2(1, 1);

	_vertices[3].Position = XMFLOAT3(_positionX, _positionY + height, 0);
	_vertices[3].Texture = XMFLOAT2(0, 1);
}
