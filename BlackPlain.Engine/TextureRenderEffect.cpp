#include "TextureRenderEffect.h"

#include "Utilities.h"

using namespace BlackPlain;

struct TextureRenderEffectMatrixBuffer
{
	XMMATRIX World;
	XMMATRIX View;
	XMMATRIX Projection;
};

TextureRenderEffect::TextureRenderEffect(IRenderer* renderer, ICamera* camera)
	: RenderEffect(renderer, camera)
{
	D3D11_INPUT_ELEMENT_DESC inputLayoutDescription[2];

	inputLayoutDescription[0].SemanticName = "POSITION";
	inputLayoutDescription[0].SemanticIndex = 0;
	inputLayoutDescription[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputLayoutDescription[0].InputSlot = 0;
	inputLayoutDescription[0].AlignedByteOffset = 0;
	inputLayoutDescription[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputLayoutDescription[0].InstanceDataStepRate = 0;

	inputLayoutDescription[1].SemanticName = "TEXCOORD";
	inputLayoutDescription[1].SemanticIndex = 0;
	inputLayoutDescription[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	inputLayoutDescription[1].InputSlot = 0;
	inputLayoutDescription[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputLayoutDescription[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputLayoutDescription[1].InstanceDataStepRate = 0;

	LoadVertexShader("TextureVertexShader.cso", inputLayoutDescription, 2);
	LoadPixelShader("TexturePixelShader.cso");

	D3D11_SAMPLER_DESC samplerDesc;

	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	_samplerStates = _renderer->CreateSamplerStates(&samplerDesc, 1);

	auto matrixBufferDesc = D3D11_BUFFER_DESC();
	ZeroMemory(&matrixBufferDesc, sizeof(D3D11_BUFFER_DESC));

	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(TextureRenderEffectMatrixBuffer);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	_matrixBuffer = _renderer->CreateBuffer(matrixBufferDesc);
}

void TextureRenderEffect::Update()
{
	TextureRenderEffectMatrixBuffer data;

	data.World = _camera->GetWorld();
	data.View = _camera->GetView();

	if (_is2d)
	{
		data.Projection = _camera->GetOrthographic();
	}
	else
	{
		data.Projection = _camera->GetProjection();
	}

	_renderer->UpdateBuffer(&data, sizeof(data), _matrixBuffer);

	_renderer->SetVertexShaderConstantBuffers(1, &_matrixBuffer);
}
