#pragma once

#include "ISceneManager.h"
#include "IRenderer.h"
#include "IUpdateable.h"

#include <vector>

namespace BlackPlain
{
	class SceneManager : public ISceneManager
	{
	public:
		SceneManager(IRenderer* renderer);

		virtual void Update();

		virtual void SetNextScene(IScene* scene) { _nextScene = scene; }

		virtual void RequestShutdown() { _isShutdownRequested = true; }

		virtual BlackPlain::IRenderer* GetRenderer() { return _renderer; }

		virtual void AddUpdateable(IUpdateable* updateable) { _updateables.push_back(updateable); }

		bool IsShutdownRequested() { return _isShutdownRequested; }

		void Shutdown();

	private:
		void ChangeCurrentScene(IScene* scene);

		IScene* _currentScene;
		IScene* _nextScene;
		bool _isShutdownRequested;
		std::vector<IUpdateable*> _updateables;
		IRenderer* const _renderer;
	};
}