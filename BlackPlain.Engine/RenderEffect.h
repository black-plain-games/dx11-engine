#pragma once

#include <d3d11.h>

#include "IRenderer.h"
#include "Camera.h"
#include "IUpdateable.h"

namespace BlackPlain
{
	class RenderEffect : public IUpdateable
	{
	public:
		RenderEffect(IRenderer* renderer, ICamera* camera);

		unsigned char GetPriority() const { return _priority; }
		void SetPriority(unsigned char priority) { _priority = priority; }

		void Apply();

		bool Is2D() { return _is2d; }
		void SetIs2D(bool is2d) { _is2d = is2d; }

	protected:
		void LoadVertexShader(const char* path, D3D11_INPUT_ELEMENT_DESC* inputLayoutDescription, unsigned int inputLayoutLength);
		void LoadPixelShader(const char* path);

		IRenderer* const _renderer;
		ICamera* const _camera;

		ID3D11SamplerState** _samplerStates;

		bool _is2d;

	private:
		ID3D11InputLayout* _inputLayout;
		ID3D11VertexShader* _vertexShader;
		ID3D11PixelShader* _pixelShader;
		unsigned int _samplerStateCount;
		unsigned char _priority;
	};
}

