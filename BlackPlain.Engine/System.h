#pragma once

#define WIN_LEAN_AND_MEAN

#include <Windows.h>
#include <dinput.h>
#include <exception>

namespace BlackPlain
{
	class System
	{
	public:
		void Initialize(HINSTANCE hInstance, int windowWidth, int windowHeight);

		inline const HWND GetWindowHandle()
		{
			return _windowHandle;
		}

		void ShowAndUpdate(int cmdShow)
		{
			ShowWindow(_windowHandle, cmdShow);
			UpdateWindow(_windowHandle);
		}

		void PollDevices();

		bool IsKeyDown(int keycode);

	private:
		static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

		HWND _windowHandle;
		LPDIRECTINPUTDEVICE8 _keyboard;
		char _keyStates[256];
	};
}