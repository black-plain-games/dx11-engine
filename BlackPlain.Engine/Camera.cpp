#include "Camera.h"

using namespace BlackPlain;

Camera::Camera(float fieldOfView, float viewWidth, float viewHeight, float nearPlane, float farPlane)
{
	_fieldOfView = fieldOfView;
	_viewWidth = viewWidth;
	_viewHeight = viewHeight;
	_nearPlane = nearPlane;
	_farPlane = farPlane;

	_up = XMFLOAT3(0, 1, 0);
	_position = XMFLOAT3(0, 0, 0);
	_target = XMFLOAT3(0, 0, 1);

	_pitch = 0;
	_roll = 0;
	_yaw = 0;

	_world = XMMatrixIdentity();
	_view = XMMatrixIdentity();
	_projection = XMMatrixIdentity();
	_orthographic = XMMatrixIdentity();

	_isWorldDirty = true;
	_isProjectionDirty = true;
	_isOrthographicDirty = true;
	_isViewDirty = true;
}

void Camera::Update()
{
	if (_isWorldDirty)
	{
		_world = XMMatrixTranspose(_world);

		_isWorldDirty = false;
	}

	if (_isProjectionDirty)
	{
		_projection = XMMatrixPerspectiveFovLH(_fieldOfView, _viewWidth / _viewHeight, _nearPlane, _farPlane);

		_projection = XMMatrixTranspose(_projection);

		_isProjectionDirty = false;
	}

	if (_isOrthographicDirty)
	{
		_orthographic = XMMatrixOrthographicLH(_viewWidth, _viewHeight, _nearPlane, _farPlane);

		_isOrthographicDirty = false;
	}

	if (_isViewDirty)
	{
		auto up = XMLoadFloat3(&_up);
		auto position = XMLoadFloat3(&_position);
		auto target = XMLoadFloat3(&_target);

		auto rotation = XMMatrixRotationRollPitchYaw(_pitch, _yaw, _roll);

		target = XMVector3TransformCoord(target, rotation);
		up = XMVector3TransformCoord(up, rotation);
		target = XMVectorAdd(target, position);

		_view = XMMatrixLookAtLH(position, target, up);

		_view = XMMatrixTranspose(_view);

		_isViewDirty = false;
	}
}

void Camera::SetFieldOfView(float fieldOfView)
{
	_fieldOfView = fieldOfView;

	_isProjectionDirty = true;
}

void Camera::SetViewDimensions(float width, float height)
{
	_viewWidth = width;
	_viewHeight = height;

	_isProjectionDirty = true;
	_isOrthographicDirty = true;
}

void Camera::SetNearPlane(float nearPlane)
{
	_nearPlane = nearPlane;

	_isProjectionDirty = true;
	_isOrthographicDirty = true;
}

void Camera::SetFarPlane(float farPlane)
{
	_farPlane = farPlane;

	_isProjectionDirty = true;
	_isOrthographicDirty = true;
}

void Camera::Rotate(float roll, float pitch, float yaw)
{
	_roll += roll;
	_pitch += pitch;
	_yaw += yaw;

	_isViewDirty = true;
}

void Camera::Rotate(XMFLOAT3 delta)
{
	Rotate(delta.x, delta.y, delta.z);
}

void Camera::MovePosition(float x, float y, float z)
{
	_position.x += x;
	_position.y += y;
	_position.z += z;

	_isViewDirty = true;
}

void Camera::MovePosition(XMFLOAT3 offset)
{
	MovePosition(offset.x, offset.y, offset.z);
}

void Camera::MoveTarget(float x, float y, float z)
{
	_target.x += x;
	_target.y += y;
	_target.z += z;

	_isViewDirty = true;
}

void Camera::MoveTarget(XMFLOAT3 offset)
{
	MoveTarget(offset.x, offset.y, offset.z);
}

void Camera::Move(float x, float y, float z)
{
	MovePosition(x, y, z);
	MoveTarget(x, y, z);
}

void Camera::Move(XMFLOAT3 offset)
{
	Move(offset.x, offset.y, offset.z);
}
