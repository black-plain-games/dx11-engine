#define WIN_LEAN_AND_MEAN

#include "Main.h"

#include <Windows.h>

#include <d3d11.h>
#include <DirectXMath.h>

#include "System.h"
#include "Renderer.h"
#include "SceneManager.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")

using namespace DirectX;
using namespace BlackPlain;

int WINAPI WinMain(
	_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ PSTR lpCmdLine,
	_In_ int nCmdShow)
{
	auto system = new System();

	auto description = Initialize();

	system->Initialize(hInstance, description.WindowWidth, description.WindowHeight);

	system->ShowAndUpdate(nCmdShow);

	auto renderer = new Renderer();

	renderer->Initialize(description.WindowWidth, description.WindowHeight, system->GetWindowHandle());
	renderer->SetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	auto sceneManager = new SceneManager(renderer);
	auto firstScene = GetFirstScene(sceneManager, system);
	sceneManager->SetNextScene(firstScene);

	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, nullptr, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			system->PollDevices();

			if (sceneManager->IsShutdownRequested())
			{
				sceneManager->Shutdown();

				break;
			}

			sceneManager->Update();

			renderer->Render();
		}
	}

	if (firstScene != nullptr)
	{
		delete firstScene;
	}

	delete sceneManager;

	renderer->Uninitialize();

	delete renderer;

	delete system;

	return int(msg.wParam);
}