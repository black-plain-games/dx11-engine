#pragma once

#include "IRenderer.h"
#include "IRenderable.h"
#include "RenderEffect.h"

#include <d3d11.h>
#include <map>
#include <vector>
#include <DirectXMath.h>

using namespace DirectX;

namespace BlackPlain
{
	class Renderer : public IRenderer
	{
	public:
		Renderer();

		void Initialize(unsigned int windowWidth, unsigned int windowHeight, HWND windowHandle);
		inline ID3D11Device* GetDevice() { return _device;  }
		inline ID3D11DeviceContext* GetDeviceContext() { return _deviceContext; }
		void Uninitialize();

		virtual void AddRenderable(RenderEffect* renderEffect, IRenderable* renderable) { _renderGroups[renderEffect].push_back(renderable); }

		virtual void SetClearColor(float color[4]);
		virtual void Render();
		virtual void SetVertexBuffer(ID3D11Buffer* buffer, unsigned int stride);
		virtual void SetIndexBuffer(ID3D11Buffer* buffer);
		virtual void SetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY topology);
		virtual void DrawIndexedPrimitive(int indexCount);

		virtual ID3D11VertexShader* CreateVertexShader(void* data, size_t length);
		virtual ID3D11InputLayout* CreateInputLayout(void* data, size_t length, D3D11_INPUT_ELEMENT_DESC* layoutDescription, unsigned int layoutLength);
		virtual ID3D11PixelShader* CreatePixelShader(void* data, size_t length);

		virtual void SetInputLayout(ID3D11InputLayout* layout);
		virtual void SetVertexShader(ID3D11VertexShader* shader);
		virtual void SetPixelShader(ID3D11PixelShader* shader);
		virtual void SetPixelShaderSamplers(unsigned int count, ID3D11SamplerState** samplers);
		virtual void SetVertexShaderConstantBuffers(unsigned int count, ID3D11Buffer** buffers);
		virtual void SetPixelShaderResources(unsigned int count, ID3D11ShaderResourceView** resources);

		virtual ID3D11SamplerState** CreateSamplerStates(D3D11_SAMPLER_DESC* descriptions, unsigned int descriptionCount);
		virtual ID3D11Buffer* CreateBuffer(D3D11_BUFFER_DESC description);
		virtual ID3D11Buffer* CreateBuffer(D3D11_BUFFER_DESC description, D3D11_SUBRESOURCE_DATA* data);
		virtual void UpdateBuffer(void* data, size_t dataLength, ID3D11Buffer* buffer);

		virtual ID3D11ShaderResourceView* CreateShaderResourceView(ID3D11Resource* resource);
		virtual ID3D11Texture2D* CreateTexture(unsigned int width, unsigned int height, unsigned char* data);
		virtual ID3D11Texture2D* CreateTexture(Utilities::ImageData& imageData);

		virtual XMFLOAT2 GetWindowDimensions() { return _windowDimensions; }

	private:
		static bool Compare(RenderEffect* a, RenderEffect* b);

		float _clearColor[4];
		XMFLOAT2 _windowDimensions;
		IDXGISwapChain* _swapChain;
		ID3D11Device* _device;
		ID3D11DeviceContext* _deviceContext;
		ID3D11Texture2D* _backBuffer;
		ID3D11RenderTargetView* _renderTargetView;
		ID3D11Texture2D* _depthStencilBuffer;
		ID3D11DepthStencilState* _depthStencilState2D;
		ID3D11DepthStencilState* _depthStencilState3D;
		ID3D11DepthStencilState* _currentDepthStencilState;
		ID3D11DepthStencilView* _depthStencilView;
		ID3D11RasterizerState* _rasterState;
		D3D11_VIEWPORT _viewport;

		std::map<RenderEffect*, std::vector<IRenderable*>, bool (*)(RenderEffect*, RenderEffect*)> _renderGroups;
	};
}