#pragma once

#include "Utilities.h"

#include <d3d11.h>
#include <DirectXMath.h>

using namespace DirectX;

namespace BlackPlain
{
	struct IRenderable;
	struct RenderEffect;

	BPINTERFACE IRenderer
	{
		virtual void AddRenderable(RenderEffect* renderEffect, IRenderable* renderable) = 0;

		virtual void SetClearColor(float color[4]) = 0;
		virtual void Render() = 0;
		virtual void SetVertexBuffer(ID3D11Buffer* buffer, unsigned int stride) = 0;
		virtual void SetIndexBuffer(ID3D11Buffer* buffer) = 0;
		virtual void SetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY topology) = 0;
		virtual void DrawIndexedPrimitive(int indexCount) = 0;

		virtual ID3D11VertexShader* CreateVertexShader(void* data, size_t length) = 0;
		virtual ID3D11InputLayout* CreateInputLayout(void* data, size_t length, D3D11_INPUT_ELEMENT_DESC* layoutDescription, unsigned int layoutLength) = 0;
		virtual ID3D11PixelShader* CreatePixelShader(void* data, size_t length) = 0;

		virtual void SetInputLayout(ID3D11InputLayout* layout) = 0;
		virtual void SetVertexShader(ID3D11VertexShader* shader) = 0;
		virtual void SetPixelShader(ID3D11PixelShader* shader) = 0;
		virtual void SetPixelShaderSamplers(unsigned int count, ID3D11SamplerState** samplers) = 0;
		virtual void SetVertexShaderConstantBuffers(unsigned int count, ID3D11Buffer** buffers) = 0;
		virtual void SetPixelShaderResources(unsigned int count, ID3D11ShaderResourceView** resources) = 0;

		virtual ID3D11SamplerState** CreateSamplerStates(D3D11_SAMPLER_DESC* descriptions, unsigned int descriptionCount) = 0;
		virtual ID3D11Buffer* CreateBuffer(D3D11_BUFFER_DESC description) = 0;
		virtual ID3D11Buffer* CreateBuffer(D3D11_BUFFER_DESC description, D3D11_SUBRESOURCE_DATA* data) = 0;
		virtual void UpdateBuffer(void* data, size_t dataLength, ID3D11Buffer* buffer) = 0;

		virtual ID3D11ShaderResourceView* CreateShaderResourceView(ID3D11Resource* resource) = 0;
		virtual ID3D11Texture2D* CreateTexture(unsigned int width, unsigned int height, unsigned char* data) = 0;
		virtual ID3D11Texture2D* CreateTexture(Utilities::ImageData& imageData) = 0;

		virtual XMFLOAT2 GetWindowDimensions() = 0;
	};
}