#include "RenderEffect.h"

#include "IRenderer.h"
#include "ICamera.h"
#include "Utilities.h"

using namespace BlackPlain;

RenderEffect::RenderEffect(IRenderer* renderer, ICamera* camera)
	: _renderer(renderer), _camera(camera)
{
	_vertexShader = nullptr;
	_pixelShader = nullptr;
	_inputLayout = nullptr;
	_samplerStates = nullptr;
	_samplerStateCount = 0;
	_priority =(unsigned char)255;
	_is2d = false;
}

void RenderEffect::LoadVertexShader(const char* path, D3D11_INPUT_ELEMENT_DESC* inputLayoutDescription, unsigned int inputLayoutLength)
{
	size_t dataLength = 0;
	auto data = (void*)Utilities::LoadFile(path, &dataLength);

	_vertexShader = _renderer->CreateVertexShader(data, dataLength);
	_inputLayout = _renderer->CreateInputLayout(data, dataLength, inputLayoutDescription, inputLayoutLength);

	delete[] data;
}

void RenderEffect::LoadPixelShader(const char* path)
{
	size_t dataLength = 0;
	auto data = (void*)Utilities::LoadFile(path, &dataLength);

	_pixelShader = _renderer->CreatePixelShader(data, dataLength);

	delete[] data;
}

void RenderEffect::Apply()
{
	_renderer->SetInputLayout(_inputLayout);
	_renderer->SetVertexShader(_vertexShader);
	_renderer->SetPixelShader(_pixelShader);
	_renderer->SetPixelShaderSamplers(_samplerStateCount, _samplerStates);
}