#pragma once

#include "RenderEffect.h"
#include "IRenderer.h"
#include "ICamera.h"

namespace BlackPlain
{
	class TextureRenderEffect : public RenderEffect
	{
	public:
		TextureRenderEffect(IRenderer* renderer, ICamera* camera);
		virtual void Update();

	private:
		ID3D11Buffer* _matrixBuffer;
	};
}