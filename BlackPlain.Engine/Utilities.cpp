#include "Utilities.h"

#include "png.h"

#pragma comment(lib, "libpng16.lib")

char* BlackPlain::Utilities::LoadFile(std::filesystem::path fileName, size_t* fileLength)
{
	std::ifstream file;

	file.open(fileName, std::ios::in | std::ios::binary);

	auto length = (unsigned int)fs::file_size(fileName);

	file.seekg(0, std::ios::beg);

	auto buffer = new char[length];

	file.read(buffer, length);
	file.close();

	(*fileLength) = length;

	return buffer;
}

constexpr auto PNG_SIGNATURE_SIZE = 8;

void userReadData(png_structp pngPtr, png_bytep data, png_size_t length)
{
	auto a = png_get_io_ptr(pngPtr);

	((std::ifstream*)a)->read((char*)data, length);
}

BlackPlain::Utilities::ImageData BlackPlain::Utilities::LoadPng(std::filesystem::path fileName)
{
	std::ifstream file;

	file.open(fileName, std::ios::in | std::ios::binary);

	auto length = (unsigned int)fs::file_size(fileName);

	file.seekg(0, std::ios::beg);

	png_byte signature[PNG_SIGNATURE_SIZE];

	file.read((char*)signature, PNG_SIGNATURE_SIZE);

	if (!file.good())
	{
		throw std::exception("Failed to read PNG signature");
	}

	if (png_sig_cmp(signature, 0, PNG_SIGNATURE_SIZE) != 0)
	{
		throw std::exception("File is not a valid PNG");
	}

	auto pngData = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);

	if (pngData == nullptr)
	{
		throw std::exception("Could not initialize PNG read struct");
	}

	auto pngInfo = png_create_info_struct(pngData);

	if (pngInfo == nullptr)
	{
		png_destroy_read_struct(&pngData, nullptr, nullptr);
		throw std::exception("Could not initialize PNG info struct");
	}

	png_set_read_fn(pngData, (png_voidp)&file, userReadData);

	png_set_sig_bytes(pngData, PNG_SIGNATURE_SIZE);

	png_read_info(pngData, pngInfo);

	unsigned short width = png_get_image_width(pngData, pngInfo);
	unsigned short height = png_get_image_height(pngData, pngInfo);
	unsigned char bitDepth = png_get_bit_depth(pngData, pngInfo);
	unsigned char channels = png_get_channels(pngData, pngInfo);
	unsigned char colorType = png_get_color_type(pngData, pngInfo);

	switch (colorType)
	{
	case PNG_COLOR_TYPE_PALETTE:
		png_set_palette_to_rgb(pngData);
		channels = 3;
		break;

	case PNG_COLOR_TYPE_GRAY:
		if (bitDepth < 8)
		{
			png_set_expand_gray_1_2_4_to_8(pngData);
			bitDepth = 8;
		}
		break;
	}

	if (png_get_valid(pngData, pngInfo, PNG_INFO_tRNS))
	{
		png_set_tRNS_to_alpha(pngData);
		channels += 1;
	}

	if (bitDepth == 16)
	{
		png_set_strip_16(pngData);
	}

	png_read_update_info(pngData, pngInfo);

	auto rowSize = width * bitDepth * channels / 8;

	auto totalDataSize = rowSize * height;

	auto data = new unsigned char[totalDataSize];

	memset(data, 0, totalDataSize);

	auto rows = new unsigned char* [height];

	for (unsigned short row = 0; row < height; row++)
	{
		rows[row] = data + (rowSize * row);
	}

	png_read_image(pngData, (png_bytepp)rows);

	delete[] rows;

	png_destroy_read_struct(&pngData, &pngInfo, nullptr);

	file.close();

	if (channels == 3)
	{
		auto totalPixels = width * height;

		auto widenedData = new unsigned char[totalPixels * 4];

		for (auto pixelIndex = 0; pixelIndex < totalPixels; pixelIndex++)
		{
			auto originalOffset = pixelIndex * 3;
			auto newOffset = pixelIndex * 4;

			widenedData[newOffset] = data[originalOffset];
			widenedData[newOffset + 1] = data[originalOffset + 1UL];
			widenedData[newOffset + 2] = data[originalOffset + 2];
			widenedData[newOffset + 3] = 255;
		}

		delete[] data;

		data = widenedData;
	}

	BlackPlain::Utilities::ImageData output;

	output.Width = width;
	output.Height = height;
	output.Data = data;

	return output;
}