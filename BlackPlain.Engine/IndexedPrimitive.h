#pragma once

#include "Utilities.h"
#include "TexturedVertex.h"
#include "IRenderable.h"
#include "IRenderer.h"
#include "IUpdateable.h"

#include <d3d11.h>

namespace BlackPlain
{
	class IndexedPrimitive : public IRenderable, public IUpdateable
	{
	public:
		IndexedPrimitive(IRenderer* renderer);
		~IndexedPrimitive();

		template <typename VertexType>
		void Initialize(VertexType* vertices, unsigned int vertexCount, unsigned long* indices, unsigned int indexCount)
		{
			_vertexCount = vertexCount;
			_indexCount = indexCount;
			_vertexSize = sizeof(VertexType) * _vertexCount;
			_vertices = (char*)vertices;

			auto indexBufferDesc = D3D11_BUFFER_DESC();
			ZeroMemory(&indexBufferDesc, sizeof(D3D11_BUFFER_DESC));

			indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
			indexBufferDesc.ByteWidth = sizeof(unsigned long) * _indexCount;
			indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			indexBufferDesc.CPUAccessFlags = 0;
			indexBufferDesc.MiscFlags = 0;
			indexBufferDesc.StructureByteStride = 0;

			D3D11_SUBRESOURCE_DATA indexData;
			indexData.pSysMem = indices;
			indexData.SysMemPitch = 0;
			indexData.SysMemSlicePitch = 0;

			_indexBuffer = _renderer->CreateBuffer(indexBufferDesc, &indexData);

			auto vertexBufferDesc = D3D11_BUFFER_DESC();
			ZeroMemory(&vertexBufferDesc, sizeof(D3D11_BUFFER_DESC));

			vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
			vertexBufferDesc.ByteWidth = sizeof(VertexType) * _vertexCount;
			vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			vertexBufferDesc.MiscFlags = 0;
			vertexBufferDesc.StructureByteStride = 0;

			D3D11_SUBRESOURCE_DATA vertexData;
			vertexData.pSysMem = vertices;
			vertexData.SysMemPitch = 0;
			vertexData.SysMemSlicePitch = 0;

			_vertexBuffer = _renderer->CreateBuffer(vertexBufferDesc, &vertexData);
		}

		void SetPosition(float x, float y, float z);
		void SetPosition(DirectX::XMFLOAT3 position);

		void Move(float x, float y, float z);
		void Move(DirectX::XMFLOAT3 offset);

		ID3D11Buffer* GetVertexBuffer() { return _vertexBuffer; }
		ID3D11Buffer* GetIndexBuffer() { return _indexBuffer; }

		virtual void Update();
		virtual void Render();

	protected:
		DirectX::XMFLOAT3 _position;
		DirectX::XMFLOAT3 _offset;
		bool _isDirty;
		ID3D11Buffer* _vertexBuffer;
		ID3D11Buffer* _indexBuffer;
		IRenderer* const _renderer;

		char* _vertices;
		unsigned int _vertexCount;
		unsigned int _vertexSize;
		unsigned int _indexCount;
	};
}
