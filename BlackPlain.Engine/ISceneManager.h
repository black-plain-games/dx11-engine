#pragma once

#include "Utilities.h"
#include "IUpdateable.h"
#include "IScene.h"
#include "IRenderer.h"

namespace BlackPlain
{
	BPINTERFACE ISceneManager : public IUpdateable
	{
		virtual void SetNextScene(IScene * scene) = 0;
		virtual void RequestShutdown() = 0;
		virtual BlackPlain::IRenderer* GetRenderer() = 0;
		virtual void AddUpdateable(BlackPlain::IUpdateable* updateable) = 0;
	};
}