#pragma once

#include <DirectXMath.h>

#include "Utilities.h"
#include "IUpdateable.h"

using namespace DirectX;

namespace BlackPlain
{
	BPINTERFACE ICamera : public IUpdateable
	{
	public:

		virtual void SetFieldOfView(float fieldOfView) = 0;
		virtual void SetViewDimensions(float width, float height) = 0;
		virtual void SetNearPlane(float nearPlane) = 0;
		virtual void SetFarPlane(float farPlane) = 0;

		virtual void Rotate(float roll, float pitch, float yaw) = 0;
		virtual void Rotate(XMFLOAT3 delta) = 0;

		virtual void MovePosition(float x, float y, float z) = 0;
		virtual void MovePosition(XMFLOAT3 offset) = 0;

		virtual void MoveTarget(float x, float y, float z) = 0;
		virtual void MoveTarget(XMFLOAT3 offset) = 0;

		virtual void Move(float x, float y, float z) = 0;
		virtual void Move(XMFLOAT3 offset) = 0;

		virtual XMMATRIX GetWorld() const = 0;
		virtual XMMATRIX GetProjection() const = 0;
		virtual XMMATRIX GetView() const = 0;
		virtual XMMATRIX GetOrthographic() const = 0;
	};
}