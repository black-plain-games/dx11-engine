#pragma once

#include "Utilities.h"

namespace BlackPlain
{
	BPINTERFACE IScene
	{
		virtual void Initialize() = 0;
		virtual void Update() = 0;
		virtual void Uninitialize() = 0;
	};
}