#pragma once

#include "Utilities.h"
#include "IRenderer.h"

namespace BlackPlain
{
	BPINTERFACE IRenderable
	{
		virtual void Render() = 0;
	};
}