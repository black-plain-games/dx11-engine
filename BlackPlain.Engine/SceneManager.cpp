#include "SceneManager.h"

BlackPlain::SceneManager::SceneManager(IRenderer* renderer)
	: _renderer(renderer)
{
	_currentScene = nullptr;
	_nextScene = nullptr;
	_isShutdownRequested = false;
}

void BlackPlain::SceneManager::Update()
{
	if (_nextScene != nullptr)
	{
		ChangeCurrentScene(_nextScene);

		_nextScene = nullptr;
	}

	_currentScene->Update();

	for (auto updateable : _updateables)
	{
		updateable->Update();
	}
}

void BlackPlain::SceneManager::Shutdown()
{
	ChangeCurrentScene(nullptr);
}

void BlackPlain::SceneManager::ChangeCurrentScene(IScene* scene)
{
	if (_currentScene != nullptr)
	{
		_currentScene->Uninitialize();

		delete _currentScene;
	}

	if (scene != nullptr)
	{
		scene->Initialize();
	}

	_currentScene = scene;
}
