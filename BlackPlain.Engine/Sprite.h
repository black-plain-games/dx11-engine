#pragma once

#include "Utilities.h"
#include "TexturedVertex.h"
#include "IRenderable.h"
#include "IRenderer.h"
#include "IUpdateable.h"
#include "IndexedPrimitive.h"

#include <d3d11.h>

namespace BlackPlain
{
	class Sprite : public IRenderable, public IUpdateable
	{
	public:
		Sprite(IRenderer* renderer);

		~Sprite();

		void Initialize();

		void SetPosition(float x, float y);

		void Move(float xOffset, float yOffset);

		void SetDimensions(float width, float height);

		void SetScale(float x, float y);

		void SetScale(float uniformScale);

		ID3D11Buffer* GetVertexBuffer() { return _vertexBuffer; }

		ID3D11Buffer* GetIndexBuffer() { return _indexBuffer; }

		virtual void Update();

		virtual void Render();

	private:
		void UpdateVertices();

		float _positionX, _positionY;
		float _width, _height;
		float _scaleX, _scaleY;
		bool _isDirty;
		ID3D11Buffer* _vertexBuffer;
		ID3D11Buffer* _indexBuffer;
		IRenderer* const _renderer;
		TexturedVertex _vertices[4];
	};
}
