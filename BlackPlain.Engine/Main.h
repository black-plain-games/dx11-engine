#pragma once

#include "IScene.h"
#include "ISceneManager.h"
#include "EngineSetupDescription.h"
#include "System.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")

extern BlackPlain::EngineSetupDescription Initialize();

extern BlackPlain::IScene* GetFirstScene(BlackPlain::ISceneManager* sceneManager, BlackPlain::System* system);