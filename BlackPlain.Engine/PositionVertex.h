#pragma once

#include <DirectXMath.h>

namespace BlackPlain
{
	struct PositionVertex
	{
		DirectX::XMFLOAT3 Position;
	};
}