#pragma once

namespace BlackPlain
{
	struct EngineSetupDescription
	{
		float WindowWidth;
		float WindowHeight;
	};
}