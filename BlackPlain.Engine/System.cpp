#include "System.h"

#include <dinput.h>
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")

GUID keyboardGuid;

bool EnumerateKeyboardsCallback(LPCDIDEVICEINSTANCE deviceInfo, void* caller)
{
	keyboardGuid = deviceInfo->guidInstance;

	return false;
}

void BlackPlain::System::Initialize(HINSTANCE hInstance, int windowWidth, int windowHeight)
{
	auto className = "bpg-window-class";

	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = nullptr;
	wcex.lpszClassName = className;
	wcex.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	if (!RegisterClassEx(&wcex))
	{
		throw new std::exception("Failed to register window class");
	}

	_windowHandle = CreateWindow(
		className,
		"bpg-window",
		WS_OVERLAPPED | WS_BORDER | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		windowWidth,
		windowHeight,
		nullptr,
		nullptr,
		hInstance,
		nullptr);

	IDirectInput8* directInput = nullptr;

	auto result = DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)(&directInput), nullptr);

	result = directInput->EnumDevices(DI8DEVCLASS_KEYBOARD, (LPDIENUMDEVICESCALLBACK)(&EnumerateKeyboardsCallback), this, DIEDFL_ATTACHEDONLY);
	
	_keyboard = nullptr;

	result = directInput->CreateDevice(keyboardGuid, &_keyboard, nullptr);
	
	result = _keyboard->SetCooperativeLevel(_windowHandle, DISCL_BACKGROUND | DISCL_EXCLUSIVE);

	_keyboard->SetDataFormat(&c_dfDIKeyboard);

	result = _keyboard->Acquire();

	memset(_keyStates, 0, 256);
}

void BlackPlain::System::PollDevices()
{
	auto result = _keyboard->GetDeviceState(256, (void**)&_keyStates);
}

bool BlackPlain::System::IsKeyDown(int keycode)
{
	return _keyStates[keycode] & 0x80 ? 1 : 0;
}

LRESULT CALLBACK BlackPlain::System::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
}