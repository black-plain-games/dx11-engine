#pragma once

#include <filesystem>
#include <fstream>
#include <exception>

#include <d3d11.h>

namespace fs = std::filesystem;

namespace BlackPlain
{
	namespace Utilities
	{
		// Decoration macros
#ifndef BPINTERFACE
#define BPINTERFACE struct __declspec(novtable)
#endif

		// Logical macros
#ifndef FAILFAST
#define FAILFAST(x, y) if (x < 0) { return y; }
#endif

#ifndef THROWFAST
#define THROWFAST(x, message) if (x < 0) { throw std::exception(message); }
#endif

#ifndef SAFERELEASE
#define SAFERELEASE(x) x->Release(); x = nullptr;
#endif

		// Classes

		struct ImageData
		{
			int Width;
			int Height;
			unsigned char* Data;
		};

		// Methods
		char* LoadFile(std::filesystem::path fileName, size_t* fileLength);

		ImageData LoadPng(std::filesystem::path fileName);

		// Values
	}
}