#include "IndexedPrimitive.h"

#include "PositionVertex.h"

using namespace BlackPlain;

IndexedPrimitive::IndexedPrimitive(IRenderer* renderer)
	: _renderer(renderer)
{
	_position = DirectX::XMFLOAT3(0, 0, 0);
	_offset = DirectX::XMFLOAT3(0, 0, 0);
	_isDirty = false;
	_vertexBuffer = nullptr;
	_indexBuffer = nullptr;
	_vertices = nullptr;
	_vertexCount = 0;
	_vertexSize = 0;
	_indexCount = 0;
}

IndexedPrimitive::~IndexedPrimitive()
{
	SAFERELEASE(_indexBuffer);
	SAFERELEASE(_vertexBuffer);
}

void IndexedPrimitive::SetPosition(float x, float y, float z)
{
	Move(x - _position.x, y - _position.y, z - _position.z);

	_position = DirectX::XMFLOAT3(x, y, z);
}

void IndexedPrimitive::SetPosition(DirectX::XMFLOAT3 position)
{
	SetPosition(position.x, position.y, position.z);
}

void IndexedPrimitive::Move(float x, float y, float z)
{
	Move(DirectX::XMFLOAT3(x, y, z));
}

void IndexedPrimitive::Move(DirectX::XMFLOAT3 offset)
{
	if (offset.x == 0 &&
		offset.y == 0 &&
		offset.z == 0)
	{
		return;
	}

	_isDirty = true;

	_offset.x += offset.x;
	_offset.y += offset.y;
	_offset.z += offset.z;
}

void IndexedPrimitive::Update()
{
	if (!_isDirty)
	{
		return;
	}

	for (auto vertexIndex = 0; vertexIndex < _vertexCount; vertexIndex++)
	{
		PositionVertex* vertex = (PositionVertex*)(_vertices + (_vertexSize * vertexIndex));

		vertex->Position.x += _offset.x;
		vertex->Position.y += _offset.y;
		vertex->Position.z += _offset.z;
	}

	_renderer->UpdateBuffer(_vertices, _vertexSize * _vertexCount, _vertexBuffer);

	_isDirty = false;
}

void IndexedPrimitive::Render()
{
	_renderer->SetVertexBuffer(_vertexBuffer, _vertexSize);
	_renderer->SetIndexBuffer(_indexBuffer);
	_renderer->DrawIndexedPrimitive(_indexCount);
}
