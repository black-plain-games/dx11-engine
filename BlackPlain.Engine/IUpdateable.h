#pragma once

#include "Utilities.h"

namespace BlackPlain
{
	BPINTERFACE IUpdateable
	{
		virtual void Update() = 0;
	};
}