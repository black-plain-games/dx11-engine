#pragma once

#include <DirectXMath.h>

using namespace DirectX;

namespace BlackPlain
{
	struct TexturedVertex
	{
		XMFLOAT3 Position;
		XMFLOAT2 Texture;
	};
}