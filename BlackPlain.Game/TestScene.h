#pragma once

#include <BlackPlain.Engine/IScene.h>
#include <BlackPlain.Engine/ISceneManager.h>
#include <BlackPlain.Engine/TextureRenderEffect.h>
#include <BlackPlain.Engine/Sprite.h>
#include <BlackPlain.Engine/System.h>

class TestScene : public BlackPlain::IScene
{
public:
	TestScene(BlackPlain::ISceneManager* manager, BlackPlain::System* system)
		: _manager(manager)
	{
		_system = system;
		_camera = nullptr;
		_spriteRenderEffect = nullptr;
		_carTexture = nullptr;
		_carTextureView = nullptr;
		_carSprite1 = nullptr;
		_carSprite2 = nullptr;
	}

	virtual void Initialize()
	{
		auto renderer = _manager->GetRenderer();

		auto dimensions = renderer->GetWindowDimensions();

		_camera = new BlackPlain::Camera(XM_PI / 4.0f, dimensions.x, dimensions.y, 0.001f, 1000.0f);

		_manager->AddUpdateable(_camera);

		_spriteRenderEffect = new BlackPlain::TextureRenderEffect(renderer, _camera);
		_spriteRenderEffect->SetIs2D(true);
		_spriteRenderEffect->SetPriority(0);

		_manager->AddUpdateable(_spriteRenderEffect);

		auto pngData = BlackPlain::Utilities::LoadPng("car.png");

		_carTexture = renderer->CreateTexture(pngData);

		delete[] pngData.Data;
		pngData.Data = nullptr;

		_carTextureView = renderer->CreateShaderResourceView(_carTexture);

		renderer->SetPixelShaderResources(1, &_carTextureView);

		_carSprite1 = new BlackPlain::Sprite(renderer);
		_carSprite1->SetPosition(860, 440);
		_carSprite1->SetDimensions(pngData.Width, pngData.Height);
		_carSprite1->Initialize();

		renderer->AddRenderable(_spriteRenderEffect, _carSprite1);
		_manager->AddUpdateable(_carSprite1);

		_carSprite2 = new BlackPlain::Sprite(renderer);
		_carSprite2->SetPosition(0, 0);
		_carSprite2->SetDimensions(pngData.Width, pngData.Height);
		_carSprite2->Initialize();

		renderer->AddRenderable(_spriteRenderEffect, _carSprite2);
		_manager->AddUpdateable(_carSprite2);

		float color[4] = { 1, 0, 0, 1 };

		_manager->GetRenderer()->SetClearColor(color);
	}

	virtual void Update()
	{
		auto speed = 1.0f;

		auto xOffset = 0.0f;
		auto yOffset = 0.0f;

		if (_system->IsKeyDown(DIK_UP))
		{
			yOffset += speed;
		}

		if (_system->IsKeyDown(DIK_DOWN))
		{
			yOffset -= speed;
		}

		if (_system->IsKeyDown(DIK_LEFT))
		{
			xOffset -= speed;
		}

		if (_system->IsKeyDown(DIK_RIGHT))
		{
			xOffset += speed;
		}

		_carSprite1->Move(xOffset, yOffset);
	}

	virtual void Uninitialize()
	{
		delete _carSprite1;
		delete _carSprite2;
		delete _spriteRenderEffect;
		delete _camera;

		SAFERELEASE(_carTextureView);
		SAFERELEASE(_carTexture);
	}

private:
	BlackPlain::ISceneManager* const _manager;
	ID3D11Texture2D* _carTexture;
	ID3D11ShaderResourceView* _carTextureView;
	BlackPlain::Sprite* _carSprite1;
	BlackPlain::Sprite* _carSprite2;
	BlackPlain::TextureRenderEffect* _spriteRenderEffect;
	BlackPlain::ICamera* _camera;
	BlackPlain::System* _system;
};