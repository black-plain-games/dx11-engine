#include "TestScene.h"

#include <BlackPlain.Engine/Main.h>
#include <BlackPlain.Engine/EngineSetupDescription.h>
#include <BlackPlain.Engine/IScene.h>
#include <BlackPlain.Engine/ISceneManager.h>
#include <BlackPlain.Engine/System.h>

BlackPlain::EngineSetupDescription Initialize()
{
	BlackPlain::EngineSetupDescription output;

	output.WindowWidth = 1920;
	output.WindowHeight = 1080;

	return output;
}

BlackPlain::IScene* GetFirstScene(BlackPlain::ISceneManager* sceneManager, BlackPlain::System* system)
{
	return new TestScene(sceneManager, system);
}